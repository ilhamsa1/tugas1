import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import HomeDosen from '@/components/HomeDosen'
import Beranda from '@/components/Mahasiswa/Beranda'
import DaftarUjian from '@/components/Mahasiswa/DaftarUjian'
import EditProfil from '@/components/Mahasiswa/EditProfil'
import MulaiUjian from '@/components/Mahasiswa/MulaiUjian'
import Profil from '@/components/Mahasiswa/Profil'
import Beranda2 from '@/components/Dosen/Beranda2'
import BuatSoal from '@/components/Dosen/BuatSoal'
import DetailSoal from '@/components/Dosen/DetailSoal'
import ListSoal from '@/components/Dosen/ListSoal'
import Apicoba from '@/components/Apicoba'
import Depan from '@/components/Depan'
import Mulai from '@/components/Mulai'
import NotFound from '@/components/NotFound'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/siswa',
      name: 'Home',
      // meta:{ title: 'siswa'},
      component: Home,
      children: [
        {
          path: '/Beranda',
          component: Beranda
        }, {
          path: '/Profil',
          component: Profil
        }, {
          path: '/EditProfil',
          component: EditProfil
        }, {
          path: '/DaftarUjian',
          component: DaftarUjian
        }, {
          path: '/MulaiUjian',
          component: MulaiUjian
        }
      ]
    }, {
      path: '/dosen',
      name: 'HomeDosen',
      component: HomeDosen,
      children: [
        {

          path: '/beranda2',
          component: Beranda2
        }, {
          path: '/buatsoal',
          component: BuatSoal
        }, {
          path: '/listsoal',
          component: ListSoal
        }, {
          path: '/detailsoal',
          component: DetailSoal
        }
      ]
    }, {
      path: '/api',
      name: 'api',
      component: Apicoba
    }, {
      path: '/tes',
      name: 'home',
      component: Depan
    },
    {
      path: '/',
      name: 'Mulai',
      component: Mulai
    },
    {
      path: '/*',
      name: 'kosong',
      component: NotFound
    }
  ]
})
